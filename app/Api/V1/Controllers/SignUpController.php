<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
         $userCheck = User::where('email', $request->email)->get();
         if(count($userCheck)<=0){
             $user = new User($request->all());
             if(!$user->save()) {
                 throw new HttpException(500);
             }

             $token = $JWTAuth->fromUser($user);
             return response()->json([
                 'status' => true,
                 'user'=>$user,
                 'token' => $token
             ], 201);
         }else{
             return response()->json(['status'=>false,'message'=>'Some one already registered by this email address'],209);
         }
    }
}
