<?php

namespace App\Api\V1\Controllers;

use App\Reseravation;
use Dingo\Api\Http\Request;

class ReseravationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reseravation::join('places','places.id','=','reseravations.place_id')
                        ->select('reseravations.id','reseravations.name','reseravations.phone','reseravations.plate','reseravations.slot_code','places.name as place_name')->get();
        if(count($reservations)>0){
            return response()->json(['status'=>true,'message'=>'Data is retrieved successfully','data'=>$reservations],200);
        }else {
            return response()->json(['status'=>false,'message'=>'Reservation is not found','data'=>[]],209);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservation = new Reseravation();
        $reservation->name = $request->name;
        $reservation->phone = $request->phone;
        $reservation->plate= $request->plate;
        $reservation->slot_code = $request->slot_code;
        $reservation->place_id = $request->place_id;
        if($reservation->save()){
            return response()->json(['status'=>true,'message'=>'Save successfully'],200);
        }
        return response()->json(['status'=>false,'message'=>'Something is not Good ):'],209);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reseravation  $reseravation
     * @return \Illuminate\Http\Response
     */
    public function show(Reseravation $reseravation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reseravation  $reseravation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reseravation $reseravation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reseravation  $reseravation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reseravation $reseravation)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reseravation  $reseravation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reseravation = Reseravation::destroy($id);
        if($reseravation){
            return response()->json(['status'=>true,'message'=>'Successfully remove']);
        }
    }
}
