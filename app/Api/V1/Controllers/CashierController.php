<?php

namespace App\Api\V1\Controllers;

use App\User;
use Dingo\Api\Http\Request;

class CashierController extends Controller
{
    //

    public function index(){
        $cashiers = User::join('places','places.id','=','users.place_id')->where('role_id',2)
            ->select('users.id','users.name','users.email','places.name as Working_place','users.tmp_pass','users.role_id','users.place_id')->get();
        if(count($cashiers)>0){
            return response()->json(['status'=>true,'message'=>'Data is retrieved','data'=>$cashiers]);
        }
        return response()->json(['status'=>false,'message'=>'data is not Found'],404);
    }

    public function  update(Request $request, $id){
         $user = User::find($id);

         $user->name = $request->name;
         $user->email = $request->email;
         $user->tmp_pass = $request->tmp_pass;
         $user->password = bcrypt($request->password);
         $user->role_id = $request->role_id;
         $user->place_id = $request->place_id;
         if($user->save()){
             return response()->json(['status'=>true,'message'=>'Update successfully'],200);
         }
   }

    public function destroy($id) {
        $user = User::destroy($id);
        return response()->json(['status'=>true,'message'=>'Deleted successfully'],200);
    }

}
