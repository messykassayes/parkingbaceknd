<?php

namespace App\Http\Controllers;

use App\ParkedCar;
use Illuminate\Http\Request;

class ParkedCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParkedCar  $parkedCar
     * @return \Illuminate\Http\Response
     */
    public function show(ParkedCar $parkedCar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ParkedCar  $parkedCar
     * @return \Illuminate\Http\Response
     */
    public function edit(ParkedCar $parkedCar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ParkedCar  $parkedCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParkedCar $parkedCar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ParkedCar  $parkedCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParkedCar $parkedCar)
    {
        //
    }
}
