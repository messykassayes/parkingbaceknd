<?php

namespace App\Api\V1\Controllers;

use App\Http\Resources\PlaceResource;
use App\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $place = Place::with('slot')->get();
        if(count($place)<=0){
            return response()->json(['status'=>false,'message'=>'Parking place is not registered yet ):','data'=>[]],404);
        }
        return response()->json(['status'=>true,'message'=>'data is retrieved successfully','data'=>$place],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $place = new Place();

        $path = 'gagadg';
        $place->cover = $path;
        $place->name = $request->name;
        if($place->save()){
            return response()->json(['status'=>true,'message'=>'Saved successfully','data'=>$place]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $place = Place::destroy($id);
        return response()->json(['status'=>true,'message'=>'Parking place is remove'],200);
    }
}
